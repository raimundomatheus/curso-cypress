/// <reference types = "cypress" />

describe("Tickets", () => {
    beforeEach(() => cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html'));


    it("Preencher todos os campos do tipo texto", ()=>{
        const firstName = "Matheus";
        const lastName = "Caldas";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("matheus.caldasufpb@gmail.com");
        cy.get("#requests").type("Aprendendo cypress");
        cy.get("#signature").type("Testando esse campo");
    });

    it("Selecionando a opção 2", () => {
       cy.get("#ticket-quantity").select("2"); 
    });

    it("Selecionando checkbox", () => {
        cy.get("#vip").check(); 
     });

     it("Selecionando radiobutton vip", () => {
        cy.get("#friend, #publication").check(); 
     });
    
    it("has ticketbox header heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("fills and reset the form", () => {
        const firstName = "Matheus";
        const lastName = "Caldas";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("matheus.caldasufpb@gmail.com");
        cy.get("#ticket-quantity").select("2"); 
        cy.get("#vip").check(); 
        cy.get("#friend, #publication").check();
        cy.get("#requests").type("Aprendendo cypress");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets. I understand that all ticket sales are final.`
        );

        cy.get("#agree").check();
        cy.get("#signature").type(fullName);
        cy.get("button[type='submit']").should("not.be.disabled");
        cy.get("button[type='submit']").click();
        cy.get('.success > p').should("contain", "Ticket(s) successfully ordered.");

    });

    it.only("Executando testes utilizando support commands", () => {
        const dados = {
            firstName: "Matheus",
            lastName: "Caldas",
            email: "matheus.caldasufpb@gmail.com"
        };

        cy.preencherCampos(dados);

        cy.get("button[type='submit']").should("not.be.disabled");
        cy.get("button[type='submit']").click();
        cy.get('.success > p').should("contain", "Ticket(s) successfully ordered.");

    });

});